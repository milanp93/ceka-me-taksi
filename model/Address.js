export const address = [{
    id: 1,
    street: 'Stare ranke',
    number: 5,
    location: {latitude: 40.8122222, longitude: -73.9246501},
    scorePerHour: {
        'h0':{
            total: 20,
            first : 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h1': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h2': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h3': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h4': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h5': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h6': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h7': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h8': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h9': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h10': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h11': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h12': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h13': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h14': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h15': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h16': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h17': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h18': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h19': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h20': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h21': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h22': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h23': {
            total: 20,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        }
    }
},{
    id: 2,
    street: 'Stare ranke',
    number: 5,
    location: {latitude: 40.8223234, longitude: -73.9246521},
    scorePerHour: {
        'h0':{
            total: 10,
            first : 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h1': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h2': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h3': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h4': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h5': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h6': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h7': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h8': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h9': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h10': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h11': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h12': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h13': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h14': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h15': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h16': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h17': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h18': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h19': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h0': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h21': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h22': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h23': {
            total: 10,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        }
    }
},{
    id: 3,
    street: 'Stare ranke',
    number: 5,
    location: {latitude: 40.8142222, longitude: -73.9346501},
    scorePerHour: {
        'h0':{
            total: 15,
            first : 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h1': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h2': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h3': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h4': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h5': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h6': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h7': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h8': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h9': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h10': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h11': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h12': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h13': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h14': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h15': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h16': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h17': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h18': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h19': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h20': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h21': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h22': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        },
        'h23': {
            total: 15,
            first: 2,
            high: 5,
            prestige: 4,
            skyline: 6,
            default: 3
        }
    }
}]