import React from 'react'
import {View,Image} from 'react-native'

const DOLLAR_IMG = require('../../assets/dollar.png');
const styles = {
	dollar: {
		width: 20,
		height: 20
	}
}

const DollarIcon = ({count}) => {
	const items = []
	for (let i = 0; i < count; i++) {
		items.push(i)
	}
	return (
		<View style={{flex:1, flexDirection:'row'}}>
			{items.map(i => (<Image key={i} source={DOLLAR_IMG} style={styles.dollar} />))}
		</View>
	);
}

export default DollarIcon