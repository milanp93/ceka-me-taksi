import React from 'react'
import {Text, View,TouchableHighlight, Modal, Image} from 'react-native'
import {Black, White} from "../../config/colors";
import DollarIcon from './DollarIcon'


const ShowAddress = ({address,handleHide}) => {
    const {modalStyle, textStyle, viewStyle, topRight, viewItemStyle} = styles;
    let scorePerHour = {}
    if (address !== null) {
        let d = new Date();
        let n = d.getHours();        
        scorePerHour = address.scorePerHour['h'+n]
    }


    return (
        <Modal
            style={modalStyle}
            animationType="slide"
            transparent={true}
            visible={address !== null}
            onRequestClose={() => {
                // required prop...
            }}
        >
            <View style={viewStyle}>
                <TouchableHighlight onPress={handleHide} style={topRight}>
                    <Image source={require('../../assets/close.png')} style={{width: 20, height: 20}} />
                </TouchableHighlight>
                <View style={{flex: 1, justifyContent: 'center', marginBottom: 5}}>
                    <Text style={{...textStyle, fontWeight: '600'}}>Total: {scorePerHour.total}</Text>
                </View>
                <View style={viewItemStyle}>
                    <DollarIcon count={4}></DollarIcon>
                    <Text style={textStyle}>Skyline: {scorePerHour.skyline}</Text>
                </View>
                <View style={viewItemStyle}>
                    <DollarIcon count={3}></DollarIcon>
                    <Text style={textStyle}>Prestige: {scorePerHour.prestige}</Text>
                </View>
                <View style={viewItemStyle}>
                    <DollarIcon count={2}></DollarIcon>
                    <Text style={textStyle}>High: {scorePerHour.high}</Text>
                </View>
                <View style={viewItemStyle}>
                    <DollarIcon count={1}></DollarIcon>
                    <Text style={textStyle}>First: {scorePerHour.first}</Text>
                </View>
                <View style={viewItemStyle}>
                    <View style={{flex:1, flexDirection:'row'}}></View>
                    <Text style={textStyle}>Unknown: {scorePerHour.default}</Text>
                </View>                    
            </View>
        </Modal>

    );
};

const styles = {
    modalStyle: {},
    viewStyle: {
        backgroundColor: White,
        alignItems: 'center',
        flexDirection: 'column',
        height: 300,
        width: 300,
        shadowColor: Black,
        shadowOffset: {width: 0, height: 4},
        shadowOpacity: 0.2,
        elevation: 2,
        marginLeft: 45,
        marginTop: 200,
        padding: 30,
        paddingTop: 20,
        position: 'relative'
    },
    topRight: {
        position: 'absolute',
        top: 10,
        right: 10
    },
    textStyle: {
        flex: 1,
        justifyContent: 'space-between',
        fontSize: 20,
        color: Black,
    },
    viewItemStyle: {
        flexDirection:'row',
        justifyContent: 'flex-end'
    }
};

export default ShowAddress;