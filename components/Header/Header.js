import React from 'react'
import {Text, View} from 'react-native'

const Header = (props) => {
    const {textStyle, viewStyle} = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.headerText}</Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#2790A1',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 70,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 4},
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'

    },
    textStyle: {
        fontSize: 20,
        color: '#fff',
    }
};

export default Header;