import React, {Component} from 'react'
import {View, Text, TouchableNativeFeedback, TouchableOpacity, PermissionsAndroid} from 'react-native'
import {White} from "../../config/colors";
import Header from "../Header/Header";
import MapView, { Marker, Polyline } from 'react-native-maps';
import {address} from "../../model/Address";
import {ShowAddress} from '../ShowAddress';
import ClusteredMapView from 'react-native-maps-super-cluster'
import googleService from '../../services/google';
import addressService from '../../services/address';
import {INITIAL_REGION, STROKE_WIDTH, STROKE_COLOR, DEFAULT_USER_LOCATION} from '../../config/map'


class TaxiMap extends Component {
    constructor(props){
        super(props);        
        
        this.state = {
            address,            
            ...this.calculateMaxMin(address),
            activeAddress: null,
            pathSteps: []
        }
    }

    setAddress = (activeAddress) => {
        this.setState(prevState => {
            return {
                activeAddress: !!activeAddress ? activeAddress : null
            }
        });
    }

    setPathSteps = (steps) => {
        this.setState(prevState => {
            return {
                pathSteps: steps
            }
        })
    }

    calculateMaxMin = (address) => {
        let d = new Date();
        let n = d.getHours();
        const firstTotalScore = address[0].scorePerHour['h'+n].total;
        let max = firstTotalScore;
        let min = firstTotalScore;

        address.forEach(marker => {
            const totalScore = marker.scorePerHour['h'+n].total;
            if(min > totalScore)
                min = totalScore;
            if(max < totalScore)
                max = totalScore;
        });

        return {
            min,
            max
        }
    }

    pytagora(locationA,locationB){
       return Math.pow(locationA.latitude - locationB.latitude,2) + Math.pow(locationA.longitude - locationB.longitude,2);
    }

    componentDidMount(){
        addressService
            .api
            .get(
                DEFAULT_USER_LOCATION,
                mAddress => {
                    this.setState(prevState => {            
                        // console.log(mAddress)            
                        let bestLocation = mAddress[0];
                        let bestLocations = [];
                        let d = new Date();
                        let n = d.getHours();
                        // let address = mAddress.filter(addres => {
                        //     return addres.scorePerHour['h'+n].total>=1
                        // });
                        let address = mAddress
                        address.forEach(pin => {
                            if(pin.scorePerHour['h'+n].total > bestLocation.scorePerHour['h'+n].total){
                                bestLocation = pin;
                            }
                        });
                        let toleration = bestLocation.scorePerHour['h'+n].total * 0.1;
                        address.forEach(pin => {
                            if(pin.scorePerHour['h'+n].total > bestLocation.scorePerHour['h'+n].total - toleration){
                                bestLocations.push(pin);
                            }
                        })
                        let bestDistance = this.pytagora(DEFAULT_USER_LOCATION, bestLocation.location);
                        bestLocations.forEach(pin => {
                            const pitag = this.pytagora(DEFAULT_USER_LOCATION, pin.location);
                            if(pitag < bestDistance){
                                bestDistance = pitag;
                                bestLocation = pin;
                            }
                        });
                        // console.log(bestLocation);
                        // let radius = 100;
                        // let hits = bestDistance / radius;
                        // // let bestInRoudIncrementer = 0;
                        // let bestInRoudLocations = [];
                        // let distancee = bestDistance / hits;
                        // let aDis = Math.abs(DEFAULT_USER_LOCATION.latitude-bestLocation.latitude) / 2;
                        // let bDis = Math.abs(DEFAULT_USER_LOCATION.longitude-bestLocation.longitude) / 2;
                        // let bestMiddleDistance = 1000;
                        // let bestMiddle=null;
                        // // for(let i=1; i<hits;i++){
                        //     address.forEach(addres => {
                        //         let pit = this.pytagora({latitude: aDis,longitude: bDis},addres.location);
                        //         if(pit < radius){
                        //             if(bestMiddleDistance>pit){
                        //                 bestMiddleDistance = pit;
                        //                 bestMiddle = addres;
                        //             }
                        //         }
                        //     })
                        // // }


                        // console.log(address.length)
                        return {                            
                            address,
                            ...this.calculateMaxMin(address),
                            bestLocation,
                            // bestMiddle
                        }
                    }, () => {
                        this.fetchPathSteps({location: DEFAULT_USER_LOCATION}, this.state.bestLocation)
                    })
                },
                error => {
                    console.log(error)
                    this.fetchPathSteps({location: DEFAULT_USER_LOCATION}, this.state.address[0])
                }
            )
    }

    fetchPathSteps = (pointA, pointB) => {
        googleService
            .api
            .getDirections(pointA, pointB)
            .then(response => {
                // get steps from response object
                const steps = googleService.utils.decode(
                    response.data.routes[0].overview_polyline.points
                )
                this.setPathSteps(steps)
            })
            .catch(error => {
                this.setPathSteps([])
                console.log(error)
            })
    }

    renderMarker = (pin) => {
        const {min, max} = this.state;
        let d = new Date();
        let n = d.getHours();
        let percent = (pin.scorePerHour['h'+n].total - min)/(max - min);
        let size = (30*percent + 20);
        let score = percent * 10;
        return (<MapView.Marker
                onPress={() => {
                    this.setAddress(pin)
                }}
                identifier={`pin-${pin.id}`}
                key={pin.longitude.toString() + pin.latitude.toString()}
                coordinate={pin.location}>
                <View style={{...styles.clusterContainer, width: size, height: size, borderRadius: size/2, backgroundColor: 'rgb('+ (255 -(255 * percent))+',255,0)'}}>
                    <Text style={{...styles.clusterText, fontSize: size/2 }}>
                        {score}
                    </Text>
                </View>
        </MapView.Marker>
        )
    }

    renderCluster = (cluster, onPress) => {
        // console.log(cluster);
        const pointCount = cluster.pointCount,
            coordinate = cluster.coordinate,
            clusterId = cluster.clusterId;
        const clusteringEngine = this.map.getClusteringEngine(),
            clusteredPoints = clusteringEngine.getLeaves(clusterId, 100);
        const {min, max} = this.state;
        let d = new Date();
        let n = d.getHours();
        let totalPercent = 0;
        let percentCounter = 0;
        clusteredPoints.forEach(point => {
            totalPercent += (point.properties.item.scorePerHour['h'+n].total - min)/(max - min);
            percentCounter++;
        });
        let percent = totalPercent / percentCounter;
        let size = (30*percent + 20);
        return (
            <MapView.Marker identifier={`cluster-${clusterId}`} coordinate={coordinate} onPress={onPress}>
                <View style={{...styles.clusterContainer,width: size, height: size, borderRadius: size/2, backgroundColor: 'rgb('+ (255 -(255 * percent))+',255,0)'}}>
                    <Text style={{...styles.clusterText, fontSize: size/2 }}>
                        {percent * 10}
                    </Text>
                </View>
            </MapView.Marker>
        )
    };

    renderPolyline = () => {
        const {pathSteps} = this.state
        return (
            <Polyline
                coordinates={pathSteps}
                strokeWidth={STROKE_WIDTH}
                strokeColors={pathSteps.map(p => STROKE_COLOR)}
            ></Polyline>
        );
    }

    render() {
        return (
            <View style={
                {flex: 1,backgroundColor: White,flexDirection: 'column'}
            }>
                <Header headerText={'Smart pickup'}/>
                <ClusteredMapView
                    ref={(r) => { this.map = r }}
                    style={{flex: 1}}
                    data={this.state.address}
                    renderMarker={this.renderMarker}
                    renderCluster={this.renderCluster}
                    initialRegion={INITIAL_REGION}
                >
                    <MapView.Marker identifier={`cluster-999`} coordinate={DEFAULT_USER_LOCATION}>
                        <View style={{...styles.clusterContainer,width: 14, height: 14, borderRadius: 7, backgroundColor: '#3E82F7', borderWidth: 2}}>
                        </View>
                    </MapView.Marker>
                    {this.renderPolyline()}
                </ClusteredMapView>
                <ShowAddress
                    address={this.state.activeAddress}
                    handleHide={() => {
                        this.setAddress(null)
                    }}
                />
            </View>
        );
    }

}

const styles = {
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
    },
    clusterContainer: {
        width: 30,
        height: 30,
        padding: 6,
        borderRadius: 15,
        borderWidth: 1,
        alignItems: 'center',
        borderColor: '#fff',
        justifyContent: 'center',
    },
    clusterText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: '200',
        textAlign: 'center',
    },
    controlBar: {
        top: 24,
        left: 25,
        right: 25,
        height: 40,
        borderRadius: 20,
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        backgroundColor: 'white',
        justifyContent: 'space-between',
    },
    button: {
        paddingVertical: 8,
        paddingHorizontal: 20,
    },
    novaLabLogo: {
        right: 8,
        bottom: 8,
        width: 64,
        height: 64,
        position: 'absolute',
    },
    text: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    counterText: {
        fontSize: 14,
        color: '#65bc46',
        fontWeight: '400'
    },
    calloutStyle: {
        width: 64,
        height: 64,
        padding: 8,
        borderRadius: 8,
        borderColor: '#65bc46',
        backgroundColor: 'white',
    },
};

export default TaxiMap;