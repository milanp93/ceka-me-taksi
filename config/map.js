export const INITIAL_REGION = {
    latitude: 40.8122222,
    longitude: -73.9246501,
    latitudeDelta: 3,
    longitudeDelta: 3
};
export const STROKE_WIDTH = 4;
export const STROKE_COLOR = '#3E82F7';

export const DEFAULT_USER_LOCATION = {
	latitude: 40.8242222,
	longitude: -73.9346501
};