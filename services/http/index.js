import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com'
});

axiosInstance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const http = axiosInstance;

export { http }
