import axios from 'axios'
import utils from './utils'
import {API_KEY} from '../../config/google'

export default {
	getDirections (pointA, pointB) {
		return axios.get(
			'https://maps.googleapis.com/maps/api/directions/json',
			{
				params: {
					origin: utils.createOrigin(pointA),
					destination: utils.createOrigin(pointB),
					key: API_KEY
				}
			}
		)
	}
}