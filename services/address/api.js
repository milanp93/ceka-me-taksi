import axios from 'axios'
import utils from './utils'

const BASE_URL = 'http://185.26.117.241:8081'

export default {
	get(location, cb, errorCb) {		
		// address/pickup-spots-0-10000
		return axios.get(`${BASE_URL}/address?size=1000`, {
			// ...location
		})
			.then(response => {
				// console.log(response.data._embedded.address)
				cb(
					response.data._embedded.address
						.map(address => {
							return {
								...address,
								location: {
									latitude: address.latitude,
									longitude: address.longitude
								}
							}
						})
				)
			})
			.catch(error => {
				errorCb(error)
			})
	}
}